import React from "react";
import { DocsThemeConfig } from "nextra-theme-docs";

const config: DocsThemeConfig = {
  logo: <h1>Portal-V6</h1>,

  nextThemes: {
    defaultTheme: "system",
  },
  project: {
    link: "https://gitlab.com/alan5432032/portal-v6-documentation-briviant",
  },
  // chat: {
  //   link: "https://discord.com",
  // },
  docsRepositoryBase: "https://github.com/shuding/nextra-docs-template",
  footer: {
    text: "Nextra Docs Template",
  },
};

export default config;
