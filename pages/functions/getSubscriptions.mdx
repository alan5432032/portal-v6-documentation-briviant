# getAllServers

The `getSubscriptions` function allows you to fetch all the servers in database given a list of Recurly Subscriptions. `getAllServers` function can only be used in server components, generally in the `page.tsx` file.

## Parameters

The `getSubscriptions` function accepts one arguments:

```ts copy
getSubscriptions(email: string): Promise<Subscription []> 
```

| Parameter| Type   | Description                    |
|----------|--------|--------------------------------|
| `email`  | string | A list of recurly subscription |


## Returns

`getSubscriptions` returns an array of Recurly Subscription object.


```ts
export declare class Subscription {
  /**
   * Subscription ID
   */
  id?: string | null;
  /**
   * Object type
   */
  object?: string | null;
  /**
   * The UUID is useful for matching data with the CSV exports and building URLs into Recurly's UI.
   */
  uuid?: string | null;
  /**
   * Account mini details
   */
  account?: AccountMini | null;
  /**
   * Just the important parts.
   */
  plan?: PlanMini | null;
  /**
   * State
   */
  state?: string | null;
  /**
   * Subscription shipping details
   */
  shipping?: SubscriptionShipping | null;
  /**
   * Returns subscription level coupon redemptions that are tied to this subscription.
   */
  couponRedemptions?: CouponRedemptionMini[] | null;
  /**
   * Subscription Change
   */
  pendingChange?: SubscriptionChange | null;
  /**
   * Current billing period started at
   */
  currentPeriodStartedAt?: Date | null;
  /**
   * Current billing period ends at
   */
  currentPeriodEndsAt?: Date | null;
  /**
   * The start date of the term when the first billing period starts. The subscription term is the length of time that a customer will be committed to a subscription. A term can span multiple billing periods.
   */
  currentTermStartedAt?: Date | null;
  /**
   * When the term ends. This is calculated by a plan's interval and `total_billing_cycles` in a term. Subscription changes with a `timeframe=renewal` will be applied on this date.
   */
  currentTermEndsAt?: Date | null;
  /**
   * Trial period started at
   */
  trialStartedAt?: Date | null;
  /**
   * Trial period ends at
   */
  trialEndsAt?: Date | null;
  /**
   * The remaining billing cycles in the current term.
   */
  remainingBillingCycles?: number | null;
  /**
   * The number of cycles/billing periods in a term. When `remaining_billing_cycles=0`, if `auto_renew=true` the subscription will renew and a new term will begin, otherwise the subscription will expire.
   */
  totalBillingCycles?: number | null;
  /**
   * If `auto_renew=true`, when a term completes, `total_billing_cycles` takes this value as the length of subsequent terms. Defaults to the plan's `total_billing_cycles`.
   */
  renewalBillingCycles?: number | null;
  /**
   * Whether the subscription renews at the end of its term.
   */
  autoRenew?: boolean | null;
  /**
   * The ramp intervals representing the pricing schedule for the subscription.
   */
  rampIntervals?: SubscriptionRampIntervalResponse[] | null;
  /**
   * Null unless subscription is paused or will pause at the end of the current billing period.
   */
  pausedAt?: Date | null;
  /**
   * Null unless subscription is paused or will pause at the end of the current billing period.
   */
  remainingPauseCycles?: number | null;
  /**
   * 3-letter ISO 4217 currency code.
   */
  currency?: string | null;
  /**
   * Revenue schedule type
   */
  revenueScheduleType?: string | null;
  /**
   * Subscription unit price
   */
  unitAmount?: number | null;
  /**
   * Determines whether or not tax is included in the unit amount. The Tax Inclusive Pricing feature (separate from the Mixed Tax Pricing feature) must be enabled to utilize this flag.
   */
  taxInclusive?: boolean | null;
  /**
   * Subscription quantity
   */
  quantity?: number | null;
  /**
   * Add-ons
   */
  addOns?: SubscriptionAddOn[] | null;
  /**
   * Total price of add-ons
   */
  addOnsTotal?: number | null;
  /**
   * Estimated total, before tax.
   */
  subtotal?: number | null;
  /**
   * Only for merchants using Recurly's In-The-Box taxes.
   */
  tax?: number | null;
  /**
   * Only for merchants using Recurly's In-The-Box taxes.
   */
  taxInfo?: TaxInfo | null;
  /**
   * Estimated total
   */
  total?: number | null;
  /**
   * Collection method
   */
  collectionMethod?: string | null;
  /**
   * For manual invoicing, this identifies the PO number associated with the subscription.
   */
  poNumber?: string | null;
  /**
   * Integer paired with `Net Terms Type` and representing the number of days past the current date (for `net` Net Terms Type) or days after the last day of the current month (for `eom` Net Terms Type) that the invoice will become past due. For `manual` collection method, an additional 24 hours is added to ensure the customer has the entire last day to make payment before becoming past due. For example:  If an invoice is due `net 0`, it is due 'On Receipt' and will become past due 24 hours after it's created. If an invoice is due `net 30`, it will become past due at 31 days exactly. If an invoice is due `eom 30`, it will become past due 31 days from the last day of the current month.  For `automatic` collection method, the additional 24 hours is not added. For example, On-Receipt is due immediately, and `net 30` will become due exactly 30 days from invoice generation, at which point Recurly will attempt collection. When `eom` Net Terms Type is passed, the value for `Net Terms` is restricted to `0, 15, 30, 45, 60, or 90`.  For more information on how net terms work with `manual` collection visit our docs page (https://docs.recurly.com/docs/manual-payments#section-collection-terms) or visit (https://docs.recurly.com/docs/automatic-invoicing-terms#section-collection-terms) for information about net terms using `automatic` collection.
   */
  netTerms?: number | null;
  /**
   * Optionally supplied string that may be either `net` or `eom` (end-of-month). When `net`, an invoice becomes past due the specified number of `Net Terms` days from the current date. When `eom` an invoice becomes past due the specified number of `Net Terms` days from the last day of the current month. 
   */
  netTermsType?: string | null;
  /**
   * Terms and conditions
   */
  termsAndConditions?: string | null;
  /**
   * Customer notes
   */
  customerNotes?: string | null;
  /**
   * Expiration reason
   */
  expirationReason?: string | null;
  /**
   * The custom fields will only be altered when they are included in a request. Sending an empty array will not remove any existing values. To remove a field send the name with a null or empty value.
   */
  customFields?: CustomField[] | null;
  /**
   * Created at
   */
  createdAt?: Date | null;
  /**
   * Last updated at
   */
  updatedAt?: Date | null;
  /**
   * Activated at
   */
  activatedAt?: Date | null;
  /**
   * Canceled at
   */
  canceledAt?: Date | null;
  /**
   * Expires at
   */
  expiresAt?: Date | null;
  /**
   * Recurring subscriptions paid with ACH will have this attribute set. This timestamp is used for alerting customers to reauthorize in 3 years in accordance with NACHA rules. If a subscription becomes inactive or the billing info is no longer a bank account, this timestamp is cleared.
   */
  bankAccountAuthorizedAt?: Date | null;
  /**
   * If present, this subscription's transactions will use the payment gateway with this code.
   */
  gatewayCode?: string | null;
  /**
   * Billing Info ID.
   */
  billingInfoId?: string | null;
  /**
   * The invoice ID of the latest invoice created for an active subscription.
   */
  activeInvoiceId?: string | null;
  /**
   * Whether the subscription was started with a gift certificate.
   */
  startedWithGift?: boolean | null;
  /**
   * When the subscription was converted from a gift card.
   */
  convertedAt?: Date | null;
  /**
   * Action result params to be used in Recurly-JS to complete a payment when using asynchronous payment methods, e.g., Boleto, iDEAL and Sofort.
   */
  actionResult?: object | null;

}
```

## Example

```tsx filename="pages.tsx" copy
import { getAllServers } from "@/utils/server/server.service";

export default function Page() {

    const session = useSession();
    const email = session.user.email;

    const subscriptions = await getSubscriptions(email);

    return (
        <div>
            {subscriptions.map((subscription) => (
                <div key={subscription.uuid}>
                    <p>{subscription.uuid}</p>
                </div>
            ))}
        </div>
    );
    } 
  return null
}
```
